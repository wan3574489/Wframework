<?php
	//数据库的配置
	//$config['db_encoding'] = 1;
	$config['db']	    = 'test';		//	<- database name
	$config['host'] 	= 'localhost';	//	<- database host
	$config['user'] 	= 'root';		//	<- database user
	$config['pass']	    = '';			//	<- database password
	$config['port']	    = '3306';		//	<- database port
	//模块注册配置
	$config['Module']	= array('defults','admin');
	//前端试图文件
	$config['view']		= array('bootstrap'=>array(
							              'css'=>false,
										  'img'=>false,
									      'js' =>false
						  ));
	return $config;