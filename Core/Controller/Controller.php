<?php
	include_once("/Controller/Request/Request.php");
	include_once("/view/View.php");
 /*
 	控制器类
 	单例模式
 	@W_Request_Abstract 负责将 Request(这里类似URL) 和 Dispatch(根据request来进行调用不同的数据)进行合作调用
 */
 class W_Controller{
	static private $instance;
	static function getInstance(){
	if(is_null(self::$instance))
		self::$instance = new W_Controller;	
		return self::$instance;
	}
	private $dispatch;					//分配类
	private $request;					//request类
	private $itemDir;					//项目目录
	private $moduleDir	=  array();		//array,数据模型放置的地点--从某一方面来讲这个应该放置在某一个类中
	/*
		需要传递一个request类
		request类进行url的调用处理,把url处理为Model,Controller,Action
	*/
	function __construct(W_Request_Abstract $request = NULL){
		 if($request == NULL){
			/*
				W_Request_Controller Request工厂模式,根据不同的情况(是通过路由模式来处理url还是通过普通的GET来处理URL)
			*/
			$request_controller = apply_filters("W_Request_Controller",W_Request_Controller::getInstance());
			/*
				$this->request  这个必须有一个继承自W_Request_Abstract的类
			*/
			$this->request = $request_controller->getRequest();	
		}else{
			$this->request = $request;
		}
		$this->initModel();
		
		$this->itemDir = dirname($_SERVER['SCRIPT_FILENAME']);	
	}
	/*
	* 载入数据模型类
	* 载入数据模型基类,根据配置文件获取到的东西来对模型基类进行设置 
	*/
	function initModel(){
		 if(!defined("_MODEL_DIR_"))
		 	define("_MODEL_DIR_",dirname(__FILE__).'/../Model/');
		//数据操作类--pdo
		include_once("/Model/Abstract.php");
		//数据模型基类
		include_once("/Model/dbClass/Abstract.php");
		//载入基本模型
		$this->registerModelDir(_MODEL_DIR_."dbClass/");
	}
	/*
	* 注册数据模型放置的地点
	* return $this
	*/
	function registerModelDir($dir){
		if(!is_dir($dir))
		  throw new W_Controller_Exception('数据模型放置的地址有误,当前地址不是文件目录!');
		$this->moduleDir[] = $dir;
		
		return $this;
	}
	/*
	* 载入数据模型,并且实例化
	* 
	*/
	function loadModel($className){
		$className = "{$className}Model";
		if(class_exists($className))
			return new $className;
		
		$module = $this->getRequest()->getModule();
				
		foreach($this->moduleDir as $dir){
			foreach(array($dir,$dir.$module."/") as $singleDir){
				$fileDir = $singleDir.$className.'.php';				
				if(file_exists($fileDir)){
					require_once($fileDir);
					if(class_exists($className))
						return new $className;
				}
			}			
									
		}
		throw new W_Controller_Exception("数据模型{$className}加载失败!");
	}
	/*
	*
	*/
	function getDefultConfig(){
		return $array = array(
			'Module'	=> array("defults")
		);	
	}
	/*
	* 加入配置文件
	* 当前的配置文件时直接引入一个php文件,以后可以使用配置文件进行解析
	* return $config; 
	*/
	function setConfig($fileName){
		if(is_file($fileName)){			
			return array_merge($this->getDefultConfig(),require($fileName));
		}
		return $this->getDefultConfig();
	}
	/*
	    获取项目根目录
	*/
	function getItemDir(){
		return $this->itemDir;	
	}
	/*
		获取Request
	*/
	function getRequest(){
		return $this->request;	
	}
	/*
	 	注册模块
	*/
	function registerModule($Module = array()){
		$this->getRequest()->registerModule($Module);
	}
	/*
	设置控制器的目录
	*/
	function setControllerDir($dir){
		$this->getDispatch()->setControllerDir($dir);
		return $this;
	}
	/*
	获取控制器的目录
	*/
	function getControllerDir(){
		return $this->getDispatch()->getControllerDir();
	}
	/*
	获取分配器dispatch
	*/
	function getDispatch(){
		if(is_null($this->dispatch)){
			include_once("/Controller/Dispatch/Dispatch.php");
			$this->getRequest()->init();
			$this->dispatch = new W_Controller_dispatch($this->getRequest());	
		}
			return $this->dispatch;		
	}
	/*
	 调用分配的数据
	*/ 
 	function dispatch(){
			$dispatch = $this->getDispatch();
			try{
				 $dispatch->dispatch();				
			}catch(Exception $e){
				   echo $e->display(); 
			}
		
	}
 }
?>