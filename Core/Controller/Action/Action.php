<?php
	class W_Controller_Action{
		public $view;
		public function __construct(){
			$this->setView()
				 ->init();	
		}
		public function init(){
				
		}
		/*
			设置Action用来展示数据的View的值
			view类是单例模式
		*/
		private function setView(){
			if(is_null($this->view)){
				include_once("/view/view.php");
				$this->view =W_View::getInstance();
			}
			return $this;
		}
		/*
			呼叫其他Controller的action
			需要一个simple类,构造一个request类,设置里面的值,然后交给dispatch类来处理
		*/
		public function callAction($Action,$Controller,$Module=NULL){
				include_once("/Controller/Request/Simple.php");	
				
				$simple = new W_Controller_Request_Simple($Action,$Controller,$Module);
				
				$dispath = new W_Controller_dispatch($simple);		
				
				$ControllerDir = W_Controller::getInstance()->getControllerDir();
				$dispath->setControllerDir($ControllerDir);
				
				$dispath->dispatch(); 
		}
		public function M($className){
			return W_Controller::getInstance()->loadModel($className);	
		}
		public function V($file=NULL){
			return W_View::getInstance()->echoRender($file);	
		}
	}
?>