<?php
	include_once("/Controller/Request/Router.php");

	/*
		Request控制类,
		工厂模式,单例模式
		这个类中的getRequest方法会返回一个request类
		
	*/
	class W_Request_Controller{
		private $Router;		
		
		static private $instance;
		static function getInstance(){
			if(is_null(self::$instance))
				self::$instance = new self;	
			return self::$instance;
		}
		/*
		 在这里进行各类判断,目前只有一个路由器模式
		 目前在测试阶段,直接是路由器模式
		*/
		function __construct(){
			if(true)
				$this->Router = true;
		}
		/*
			返回是否是路由器模式
		*/
		function is_Router(){
			return $this->Router;	
		}
		/*
			不同的值返回不同的值
			目前是测试阶段,直接返回一个路由模式request类
		*/		
		function getRequest(){
			if($this->is_Router()){
				return new W_Request_Router();	
			}
		}
		
	}
?>