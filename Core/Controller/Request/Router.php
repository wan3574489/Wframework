<?php
	include_once("/Controller/Request/Abstract.php");
	/*
		路由器模式
		主要是设置Module,Controller,Action的值,方便调用的
	*/
	class W_Request_Router extends W_Request_Abstract{
			/*设置模块,控制,动作值在param中的key*/			
			function setKey(){
				if($this->getModuleStatus()){
					$this->ModuleKey 		=  0;						
					$this->ControllerKey    =  1;
					$this->ActionKey		=  2;
				}else{
					$this->ModuleKey 		=  -1;						
					$this->ControllerKey    =  0;
					$this->ActionKey		=  1;
				}				
			}
			function setValue(){
				//模块处理
				if($this->getModuleStatus()){
					$module = $this->getParam($this->ModuleKey);
					//当前url存在模块的值
					if($module){
						//当前获取到的模块已经被注册,设置当前正确的模块
						//var_dump($this->ModuleName);
						if(in_array($module,$this->ModuleName))
							$this->Module = $module;	
						else{
							//当前模块没有被注册,表示当前是默认的模块
							$this->ModuleKey = -1;
							$this->ControllerKey    =  0;
							$this->ActionKey		=  1;	
						}
					}
				}
				$this->Controller = ($this->getParam($this->ControllerKey)==false)?$this->Controller:$this->getParam($this->ControllerKey);
				$this->Action = ($this->getParam($this->ActionKey)==false)?$this->Action:$this->getParam($this->ActionKey);
			}
			/*
				获取当前request
				进行判断
				去除掉所有的物理地址
			*/
			function init(){
				$urlPrefix = $this->urlPrefix();
				$request   = $_SERVER['REQUEST_URI'];
	
				//移除index.php
				$request = str_replace($_SERVER['SCRIPT_NAME'],'',$request);
				
				//替换掉二级目录地址,如果有的话
				if($urlPrefix){
				    $request =str_replace($urlPrefix,'',$request);				
				}
				$this->param = $this->urlsplit('/',$request);
				
				$this->setKey();
				$this->setValue();			
			}
			/*
				返回url中部分是物理地址的参数-如果有的话
			*/
			function urlPrefix(){
				$self = $this->urlsplit('/',$_SERVER['PHP_SELF']);	
			
				if(count($self)<=1)
					return false;				
				$self_replace = str_replace($self[count($self)-1],'',$_SERVER['PHP_SELF']);
				
				return $self_replace;				
			}		
			/*
				url地址的切割
			*/
			function urlsplit($pattern,$subject){
				return preg_split("|{$pattern}|",$subject,-1, PREG_SPLIT_NO_EMPTY);	
			}
	}
?>