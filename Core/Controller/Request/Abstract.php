<?php
	include_once('/Controller/Request/Interface.php');
	/*
	  抽象类--request基类		
	*/
	abstract class W_Request_Abstract implements W_Request_Interface{
			protected   $ModuleName		= array()  ;		 //注册模块的值
			protected  $Module			= 'home';		 //默认模块的值
			public  $ModuleKey;	 						 	 //模块前缀的值
			protected  $Controller		= 'index';		     //控制器的值
			public  $ControllerKey;  
			protected  $Action			= 'index';			 //具体某个方法
			public  $ActionKey;     					 	
			protected  $param;		     					 //参数
		
		function __construct(){
				//$this->init();		
		}	
		/*
		 设置参数当前数据请求中的参数		
		*/
		function getParam($key){
		   if(isset($this->param[$key])){
			    return $this->param[$key]; 
		   }
				return false;
		}
		/*
			设置当前数据请求中的参数
			无返回的值,原来参数会覆盖
		*/
		function setParam($key,$value){
			$this->param[$key] = $value;
		}
		/*
			返回当前Module模块的值
		*/
		function getModule(){
				return $this->Module;	
		}
		/*
			返回当前控制器值
		*/
		function getController(){
			return $this->Controller;	
		}
		/*
			返回当前action的值
		*/
		function getAction(){
			return $this->Action;	
		}
		/*
			当前是否开启模块功能
			目前是在测试阶段,直接返回false
			以后肯定需要加入一些判断的
		*/
		function getModuleStatus(){
			
			return !empty($this->ModuleName)?true:false;
		}
		/*
		* 注册模块,
		* $module必须是一个数组,否则无法注册成功
		* return bool
		*/
		function registerModule($Module){
			if(is_array($Module)){
				$this->ModuleName = $Module;	
			}
			else
				return false;
			
			return true; 
		}
		/*当前的模块是什么*/
		function setValue(){
			//如果param参数中木有任何东东就可以退出了,可能会感觉多此一举,都是能避免资源的重复
			if((!$this->getParam($this->ModuleKey) && $this->getModuleStatus()) 
			||(!$this->getParam($this->ControllerKey) && !$this->getModuleStatus()))
						return;
				
				$this->Module = ($this->getParam($this->ModuleKey)==false)?$this->Module:$this->getParam($this->ModuleKey);
				$this->Controller = ($this->getParam($this->ControllerKey)==false)?$this->Controller:$this->getParam($this->ControllerKey);
				$this->Action = ($this->getParam($this->ActionKey)==false)?$this->Action:$this->getParam($this->ActionKey);
				
		}
	}
?>