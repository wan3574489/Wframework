<?php
	include_once('/Controller/Exception.php');
	include_once('/Core/Controller/Action/Action.php');
	/*
		派遣类---现在就这样说吧
		通过@request类中的Module,Controller,Action来调用客户端代码生成的类中的方法
		
	*/
	class W_Controller_dispatch{
		private $ControllerDir;
		private $request;

		function __construct(W_Request_Abstract $request){
			$this->request = $request;
		}
		/*
			设置客户端控制器所在的目录
		*/
		function setControllerDir($dir){
			if(!is_string($dir)){
				throw new W_Controller_Exception('控制器目录必须是字符串!');
			}
			$this->ControllerDir = $dir;
		}
		/*
			获取客户端控制器所在的目录
		*/
		function getControllerDir(){
			return $this->ControllerDir;	
		}
		/*
			开始调用Module中Controller中的Action
		*/
		function dispatch(){
			/*
				Controller的名称需要处理一下
				这里变成了 $Controller首字母大写+Controller
			*/
			 $ControllerName  = $this->formatController($this->request->getController());
			 $Action   		  = $this->formatAction($this->request->getAction());
			/* 	载入文件	*/		
			$this->loadClass($ControllerName);			
			//实例化
			$Controller       = new $ControllerName;
			//必须继承自W_Controller_Action
			if(!$Controller instanceof W_Controller_Action){
				throw new W_Controller_Exception("控制器{$ControllerName}没有继承W_Controller_Action");	
			}
			//Controller中是否有这个方法action
			if(!method_exists($Controller,$Action)){
				throw new W_Controller_Exception("制作器{$ControllerName}中没有方法{$Action}");	
			}
			//调用
			$Controller->$Action();
		
		}
		/*
			载入类的名称
			通过是否开启了模块Module来载入不同的文件
		*/
		function loadClass($className){			
			if($module = $this->getModuleName()){
				$File_dir = $this->ControllerDir.'/'.$module.'/'.$className.'.php';				
			}else{
				$File_dir = $this->ControllerDir.'/'.$className.'.php';				
			}
			
			if(!file_exists($File_dir)){			
					throw new W_Controller_Exception("控制器{$className}没有发现在控制器目录($File_dir)中");
			}
			include_once($File_dir);			
		}
		/*
			获取模块地址
		*/
		function getModuleName(){
			if(!$this->request->getModuleStatus())	
				return false;
			return $this->request->getModule();
		}
		/*
			格式化字符串
			把传入的字符串首字母大写
		*/	
		function formatString($string){
			return ucfirst($string);
		}
		/*
			格式化控制器名称
			return 首字母大写的控制器.Controller
		*/
		function formatController($Controller){
			return $this->formatString($Controller).'Controller';
		}
		/*
			格式化Action名称
			return 同上
		*/
		function formatAction($Action){
			return $this->formatString($Action).'Action';
		}
	}
?>