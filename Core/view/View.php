<?php
	/*
	* 视图脚本
	*
	*/
	class W_View{
		static public $instance;
		static public function getInstance(){
			if(is_null(self::$instance)){
				self::$instance = new self;	
			}
			return self::$instance;
		}
		
		private $ViewDir;			//试图脚本地址目录
		private $file;				//当前载入的文件
		private $Controller;		//当前控制器
		private $Request;			//当前request
		private $ViewList=array(	//试图脚本的种类及地址
			'script'=>false,		//模块脚本
			'helper'=>false,		//试图助手
			'front'=>array(
				'js'=>false,'css'=>false,'image'=>false
				)			//前端文件放置的地方
		);
		private $ApplicateDir;		//程序根目录
		private $suffix = '.phtml';
		
		public function __construct(){
			$this->init();			
		}
		private function init(){				
				$this->Controller = W_Controller::getInstance();
				$this->Request    = $this->Controller->getRequest();
				$this->ApplicateDir = $this->Controller->getItemDir();				
		}
		/*
		* 添加一个前端试图类型
		* 
		*/
		public function addViewList($arg){
			$this->ViewList = array_merge($this->ViewList,$arg);
			return $this;	
		}
		/*
		* 获取试图
		*/
		public function showView($key,$file,$echo=true){
			$dir = $this->getViewListValue($key);
			if(!is_null($dir)){
				if($echo){
					echo $dir.'/'.$file;  
					return;
				}
				else
					return $dir.'/'.$file;
			}
			throw new W_Controller_Exception("视图脚本获取为空");
		}
		/*设置script的dir*/
		public function setScriptDir($dir=NULL){
			$this->setViewLisetDir('script',$dir);
			return $this;
		}
		public function getScriptDir(){
			return $this->getViewListValue('script');
		}
		/*设置helper的dir*/
		public function setHelperDir($dir=NULL){
			$this->setViewLisetDir('helper',$dir);
			return $this;
		}
		public function getHelperDir(){
			return $this->getViewListValue('helper');
		}
		/*
			设置ViewList的值
			$key  ViewList的key
			$dir  对应ViewList的key,如果dir为空就用默认的值,默认的值:$this->ViewDir.$key;
		*/
		public function setViewLisetDir($key,$dir = NULL){
			if(!is_null($dir)){
					$this->setViewLisetValue($key,$dir);
			}else{
				if(!is_null($this->getViewDir())){
					if(is_array($key)){
						$dir = $this->getViewDir().'/'.$key[0].'/'.$key[1];	
					}else{
						$dir = 	$this->getViewDir().'/'.$key;
					}
					$this->setViewLisetValue($key,$dir);	
				}
			}
			return $this;
		}
		/*
		*设置ViewList的值
		*如果key是数组就获取$this->viewList中的key[0]key[1]的值
		*return $this;	
		*/
		public function setViewLisetValue($key,$value){
			if(is_array($key)){
				if(isset($this->ViewList[$key[0]][$key[1]]))
					$this->ViewList[$key[0]][$key[1]] = $value;
			}else{
				if(isset($this->ViewList[$key]))
					$this->ViewList[$key] = $value;
			}
			return $this;
		}
		/*
			获取ViewList的值
		*/
		public function getViewListValue($key){
			if(is_array($key)){
				if(isset($this->ViewList[$key[0]][$key[1]]) && $this->ViewList[$key[0]][$key[1]] != false)
					return $this->ViewList[$key[0]][$key[1]];
			}else{
				if(isset($this->ViewList[$key]) && $this->ViewList[$key] != false)
					return $this->ViewList[$key];			
			}
			return false;
		}
		/*
			获取脚本地址
		*/
		public function getViewDir(){
			return $this->ViewDir;
		}
		/*
			载入默认的script和helper地址
		*/
		public function setViewListDir(){			
			foreach($this->ViewList as $key=>$view){
					if(is_array($view)){
						foreach($view as $k=> $v){							 
							$dirKey = array($key,$k);
							if(!$this->getViewListValue($dirKey)){
								$this->setViewLisetDir($dirKey,NULL);	
							}
						}
					}else{
						if(!$this->getViewListValue($key)){
							$this->setViewLisetDir($key,NULL);	
						}
					}
			}
		}
		/*
			设置试图脚本地址
			如果是第一次载入就载入script的地址和helper的地址
		*/
		public function setViewDir($dir){
			if(!is_string($dir)){
				throw new W_Controller_Exception("视图目录必须是一个字符串!");
			}
			$this->ViewDir = $dir;
			//载入script的地址和helper的地址
			$this->setViewListDir();
			
			return $this;
		}
		
		/*
			过瘾载入的文件的数据
		*/
		public function render($name=NULL){
			$this->scriptHtmlAddress($name);
			$this->file = $this->scriptHtmlAddress($name);
			/*
				如果name为空,说明是在最后载入默认的模板文件,不需要一定载入
				如果name不为空,说明用户一定要载入一个模板文件,没有找到就需要抛出错误
			*/
			if(($this->file == false ) && !is_null($name)){
				throw new W_Controller_Exception("没有找到需要的模板文件{$name}!");
			}
			
			if($this->isTemplateFile($this->file)){
				ob_start();			
				$this->loadHtml($this->file);				
				return $this->filter(ob_get_clean()); // filter output
			}
		}			
		/*
			生成地址
		*/
		private function scriptHtmlAddress($name=NULL){
			$Address1  = $this->ApplicateDir.$this->getScriptDir().'/'.$name;					//简写
			$Address2  = $this->ApplicateDir.$this->getScriptDir().'/'.$name.$this->suffix;    //简写+指定的后缀
						
			$Address3  = '/'.$this->Request->getController().'/'.$this->Request->getAction().$this->suffix;		//当前request生成的地址				
			$Address4  = '/'.$this->Request->getController().'/'.$name.$this->suffix;	
			$Address5  = '/'.$this->Request->getController().'/'.$name;
				
			if($this->Request->getModuleStatus()){
				$Address3 ='/'.$this->Request->getModule().$Address3;	
				$Address4 ='/'.$this->Request->getModule().$Address4;
				$Address5 ='/'.$this->Request->getModule().$Address5;
			}				
			$Address3 = $this->ApplicateDir.$this->getScriptDir().$Address3;
			$Address4 = $this->ApplicateDir.$this->getScriptDir().$Address4;
			$Address5 = $this->ApplicateDir.$this->getScriptDir().$Address5;
		
			if(is_null($name) && $this->isTemplateFile($Address3)){
				return $Address3;			
			}
			
			if($this->isTemplateFile($Address4))
				return $Address4;
			if($this->isTemplateFile($Address5))
				return $Address5;
			if($this->isTemplateFile($Address1))
				return $Address1;
			if($this->isTemplateFile($Address2))
				return $Address2;	
			
			return false;			
		}
		/*
			是否是模板文件
		*/
		private function isTemplateFile($file){
			if(file_exists($file) && !is_dir($file))
				return true;
			return false;
		}
		/*
			载入文件
		*/
		public function loadHtml(){
			include func_get_arg(0);	
		}
		/*
			数据过滤器
		*/
		public function filter($output){
			return $output;
		}
		/*输入载入的文件*/
		public function echoRender($name=NULL){
			echo $this->render($name);
		}
		/*		
		*/
		function __destruct(){
			
		}
	}
?>