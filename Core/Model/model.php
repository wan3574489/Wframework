<?php
	define("_MODEL_DIR_",dirname(__FILE__));	
	//载入数据模型抽象类,PDO
	include("Abstract.php");
	include(_MODEL_DIR_."/dbClass/Abstract.php");
	//注册自动载入器
	spl_autoload_register(function($className){
		$fileDir = _MODEL_DIR_."/dbClass/class.".$className.".php";
		if(file_exists($fileDir)){
			include_once($fileDir);			
			return true;
		}
		return false;
	});
	
?>