<?php
/**
* <b>SetFilter</b>
* 
* Specify the attributes returned by GetFilter and GetJSON plugin calls, 
* as well as the return format (key/value pair, or just value) and order. 
* 
* NOTE: Returned values occur in the exact order attribute names occur in
*   the filtering array. This allows, for instance, to reliably pipe the
*   result to a datagrid/spreadsheet using named columns. 
* 
* NOTE: Be sure to include the "__class__Id" attribute in your filter array
* 	if you would like it (the "primary key") included in the result.
*
* NOTE: Consecutive calls to SetFilter reset all existing settings.
* 
* TODO: usage.
*
* @author Brice Burgess <bhb@iceburg.net>
* @version 0.1
* @copyright Free for personal & commercial use. (Offered under the BSD license)
*
* @param array $attributeNames {"attributeName", "attributeName", ...} || {} (empty) for ALL Attributes
* @param boolean $includeAttributeName true
* @return void
*/
class SetFilter
{
	private $sourceObject;
	private $argv;
	public $version = '0.1';

	function Version()
	{
		return $this->version;
	}

	function SetFilter($sourceObject,$argv)
	{
		$this->sourceObject = $sourceObject;
		$this->argv = $argv;
	}

	function Execute()
	{
		
		$this->sourceObject->_filter = array(
			'attributes' => array(),
			'keyValue' =>  (isset($this->argv[1]) && $this->argv[1] === false) ? false : true
		);
		
		if (!empty($this->argv[0]))
		{
			
			// reserve a list of "known" valid attribute names
			$validAttributes = array();
			foreach($this->sourceObject->pog_attribute_type as $name => $a) {
				if($a['db_attributes'][0] == 'OBJECT' && $a['db_attributes'][1] == 'BELONGSTO')
					$name .= 'id'; // parent/child relationship
				
				// normalize to lowercase
				$validAttributes[] = strtolower($name);
			}
			
			
			// loop through passed names and only push known attributes to the return array 
			foreach($this->argv[0] as $attribute) 
			{
				// normalize to lowercase
				$name = strtolower($attribute);
				
				if(in_array($name,$validAttributes))
					$this->sourceObject->_filter['attributes'][] = $name;
				else
					trigger_error('SetFilter => '.$attribute.' is an invalid attribue of '.get_class($this->sourceObject).'. If this is a relationship attribute, try using "'.$attribute.'id". Ignored.');
			}
		}
		
		return;
	}

	function SetupRender()
	{
		
		echo '<p>Enables the ability to specify which attributes are returned by a GetJSON plugin call and to set the return format (return key/value pair, or just value).</p>';
		
		
		if ($this->PerformUnitTest() === false)
		{
			echo get_class($this).' failed unit test';
		}
		else
		{
			echo get_class($this).' passed unit test';
		}
	}

	function AuthorPage()
	{
		return 'http://iceburg.net/';
	}

	function PerformUnitTest()
	{
		$objectNames = unserialize($_SESSION['objectNameList']);

		//try getting a count
		if (sizeof($objectNames) > 0)
		{
			$anyObject = $objectNames[0];
			echo "using class.".strtolower($anyObject).".php <br>";
			
			include_once("../objects/class.".strtolower($anyObject).".php");
			$anyObject = new $anyObject();


			$anyObject->SetFilter();
			
			if(!isset($anyObject->_filter) || !is_array($anyObject->_filter['attributes']))
			{
				echo 'SetFilter failed to initialize';
				return false;
			}
			$anyObject->SetFilter(array(strtolower(get_class($anyObject)).'Id'),false);
			
			if(count($anyObject->_filter['attributes']) != 1 || $anyObject->_filter['keyValue'] !== false) 
			{
				echo 'SetFilter failed to respond to parameters';
				return false;
			}
			
			return true;
		}
	}
}
?>