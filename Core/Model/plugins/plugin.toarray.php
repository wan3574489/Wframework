<?php
class toArray
{
	var $sourceObject;
	var $argv;
	var $version = '1.0';

	function Version()
	{
		return $this->version;
	}

	function toArray($sourceObject, $argv)
	{
		$this->sourceObject = $sourceObject;
		$this->argv = $argv;
	}

	function Execute()
	{
		return $this->object2array($this->sourceObject);
	}

	function SetupRender()
	{
		return null;

	}

	function AuthorPage()
	{
		return null;
	}


	function SetupExecute()
	{
		return null;

	}
	
	function object2array($obj) {
    	$_arr = is_object($obj) ? get_object_vars($obj) : $obj;
	    foreach ($_arr as $key => $val) {
        	$val = (is_array($val) || is_object($val)) ? $this->object2array($val) : $val;
    	    $arr[$key] = $val;
	    }
    	return $arr;
	}

}
