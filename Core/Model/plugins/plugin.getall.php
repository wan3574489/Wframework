<?php 
class GetAll
{
	
	public $sourceObject;
	public $argv;

	function Version() {
		
		return 1.0;
		
	}

	function GetAll($sourceObject, $argv)
	{
		$this->sourceObject = $sourceObject;
		$this->argv = $argv;
	}
	
	/* @function GetAll
	 * @param array $fcvo_array field column value operator array. an array such as array(array('field', 'column', 'value','operator')[, array('field', 'column', 'value','operator')]))
	 * @param string $sort_by
	 * @param bool $ascending
	 * @param string $limit
	*/
	function Execute() {
		
		$connection = Database::Connect();
		$sqlLimit = (@$this->argv[3] != '' ? "LIMIT {$this->argv[3]}" : '');
		$this->pog_query = "select * from `projects` ";
		$projectsList = Array();
		if (sizeof($this->argv[0]) > 0)
		{
			$this->pog_query .= " where ";
			for ($i=0, $c=sizeof($this->argv[0]); $i<$c; $i++)
			{
				if (sizeof($this->argv[0][$i]) == 1)
				{
					$this->pog_query .= " ".$this->argv[0][$i][0]." ";
					continue;
				}
				else
				{
					if ($i > 0 && sizeof($this->argv[0][$i-1]) != 1)
					{
						$this->pog_query .= " {$this->argv[0][$i-1][3]} ";
					}
					if (isset($this->pog_attribute_type[$this->argv[0][$i][0]]['db_attributes']) && $this->pog_attribute_type[$this->argv[0][$i][0]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$this->argv[0][$i][0]]['db_attributes'][0] != 'SET')
					{
						if ($GLOBALS['configuration']['db_encoding'] == 1)
						{
							$value = POG_Base::IsColumn($this->argv[0][$i][2]) ? "BASE64_DECODE(".$this->argv[0][$i][2].")" : "'".$this->argv[0][$i][2]."'";
							$this->pog_query .= "BASE64_DECODE(`".$this->argv[0][$i][0]."`) ".$this->argv[0][$i][1]." ".$value;
						}
						else
						{
							$value =  POG_Base::IsColumn($this->argv[0][$i][2]) ? $this->argv[0][$i][2] : "'".$this->Escape($this->argv[0][$i][2])."'";
							$this->pog_query .= "`".$this->argv[0][$i][0]."` ".$this->argv[0][$i][1]." ".$value;
						}
					}
					else
					{
						$value = POG_Base::IsColumn($this->argv[0][$i][2]) ? $this->argv[0][$i][2] : "'".$this->argv[0][$i][2]."'";
						$this->pog_query .= "`".$this->argv[0][$i][0]."` ".$this->argv[0][$i][1]." ".$value;
					}
				}
			}
		}
		if (@$this->argv[1] != '')
		{
			if (isset($this->pog_attribute_type[$this->argv[1]]['db_attributes']) && $this->pog_attribute_type[$this->argv[1]]['db_attributes'][0] != 'NUMERIC' && $this->pog_attribute_type[$this->argv[1]]['db_attributes'][0] != 'SET')
			{
				if ($GLOBALS['configuration']['db_encoding'] == 1)
				{
					$this->argv[1] = "BASE64_DECODE({$this->argv[1]}) ";
				}
				else
				{
					$this->argv[1] = "{$this->argv[1]} ";
				}
			}
			else
			{
				$this->argv[1] = "{$this->argv[1]} ";
			}
		}
		else
		{
			$this->argv[1] = "projectsid";
		}
		$this->pog_query .= " order by ".$this->argv[1]." ".(@$this->argv[2] ? "asc" : "desc")." $sqlLimit";
		$thisObjectName = get_class($this->sourceObject);
		$cursor = Database::Reader($this->pog_query, $connection);
		exit($this->pog_query);
		while ($row = Database::Read($cursor))
		{
			$projects = new $thisObjectName();
			$projects->projectsId = $row['projectsid'];
			$projects->name = $this->Unescape($row['name']);
			$projects->detail = $this->Unescape($row['detail']);
			$projects->date_creation = $row['date_creation'];
			$projectsList[] = $projects;
		}
		return $projectsList;
		
	}

	function SetupRender() {
		
		return null;
		
	}


	function AuthorPage() {
		
		return null;
		
	}
}
?>