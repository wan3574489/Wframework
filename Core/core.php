<?php
	(!defined("APP_NAME") || !defined("APP_DIR")) &&exit;
	
	set_include_path('.'.PATH_SEPARATOR.'./Core'.PATH_SEPARATOR.PATH_SEPARATOR . get_include_path());
	include_once("/plugin/plugin-hooks.php");
	include_once("/function.php");
	//载入系统插件
	Hooks::getInstance()->register(APP_DIR."/Core/plugin/");
	
	include("Core/Controller/Controller.php");
  	$_prefix = APP_TOP."/".APP_NAME;
	
	$View       = apply_filters("W_View",W_View::getInstance());
	//获取控制器模块
	$Controller = apply_filters("W_Controller",W_Controller::getInstance());
	
	$config = $Controller ->setConfig($_prefix."/config.php");
	//注册当前有多少个模块
	$Controller->registerModule($config['Module']);	
	/*
	* 视图操作
	* 系统中已经存在
	*	 $ViewList=array(	//试图脚本的种类及地址
	*		'script'=>false,		//模块脚本
	*		'helper'=>false,		//试图助手
	*		'front'=>array(
	*			'js'=>false,'css'=>false,'image'=>false
	*			)			//前端文件放置的地方
	*	);
	*可以注册一些为视图服务的文件及文件夹
	*/
	$View->addViewList($config['view'])
		 ->setViewDir($_prefix."/view");

	try{	
		 $Controller->registerModelDir($_prefix."/Model/")
					->setControllerDir($_prefix."/controllers/");
		
		$Controller ->dispatch();
	}catch(Exception $e){
			echo $e->getMessage().'在'.$e->getFile().'的'.$e->getLine(); 
	}
	
?>