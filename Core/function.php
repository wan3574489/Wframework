<?php
	function add_filter($tag, $function_to_add, $priority = 10, $accepted_args = 1){
		 return Hooks::getInstance()->add_filter($tag, $function_to_add, $priority , $accepted_args);
	}
	function apply_filters($tag, $value){
	     return Hooks::getInstance()->apply_filters($tag, $value);
	}